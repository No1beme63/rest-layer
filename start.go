package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/render"
	"io/ioutil"
	"log"
	"os/exec"
)

const (
	STDOUT = "stdout"
	STDERR = "stderr"

	JSON = "json"
	YAML = "yaml"
	XML  = "xml"
)

type General struct {
	Routes        string `json:"routes"`
	Port          int    `json:"port"`
	Communication string `json:"communication"`
	Swagger       string   `json:"swagger"`
}

type Group struct {
	Group  string  `json:"group"`
	Routes []Route `json:"routes"`
}

type Route struct {
	Method   string            `json:"method"`
	Endpoint string            `json:"endpoint"`
	Command  string            `json:"command"`
	Path     map[string]string `json:"path"`
	Query    map[string]string `json:"query"`
	Payload  string            `json:"payload"`
	Output   string            `json:"output"`
}

func parse(path string) (Application, error) {
	file, err := ioutil.ReadFile(path)
	if err != nil {
		return Application{}, err
	}
	var config General
	err = json.Unmarshal(file, &config)
	if err != nil {
		return Application{}, err
	}
	file, err = ioutil.ReadFile(config.Routes)
	if err != nil {
		return Application{}, err
	}
	var groups []Group
	err = json.Unmarshal(file, &groups)
	if err != nil {
		return Application{}, err
	}
	return Application{
		Groups:        groups,
		Port:          config.Port,
		Communication: config.Communication,
		Swagger:       config.Swagger,
	}, nil
}

type Application struct {
	Groups        []Group
	Port          int
	Engine        *gin.Engine
	EngineGroup   *gin.RouterGroup
	Communication string
	Swagger       string
}

func (a *Application) start() {
	a.Engine = gin.Default()

	a.Engine.Static("/swaggerui/", a.Swagger)

	for _, group := range a.Groups {
		a.applyGroup(group.Group, group.Routes)
	}
	a.Engine.Run(fmt.Sprintf(":%d", a.Port)) // listen and serve on 0.0.0.0:8080 (for windows "localhost:8080")
}

func (a *Application) applyGroup(group string, routes []Route) {
	a.EngineGroup = a.Engine.Group(group)
	for _, route := range routes {
		switch route.Method {
		case "GET":
			a.GET(route)
		case "POST":
			a.POST(route)
		case "DELETE":
			a.DELETE(route)
		case "PUT":
			a.PUT(route)
		case "HEAD":
			a.HEAD(route)
		case "OPTIONS":
			a.OPTIONS(route)
		case "PATCH":
			a.PATCH(route)
		}
	}
}

func (a *Application) GET(route Route) {
	a.EngineGroup.GET(buildPath(route.Endpoint, route.Path), func(c *gin.Context) {
		a.handle(route, c)
	})
}

func (a *Application) POST(route Route) {
	a.EngineGroup.POST(buildPath(route.Endpoint, route.Path), func(c *gin.Context) {
		a.handle(route, c)
	})
}

func (a *Application) DELETE(route Route) {
	a.EngineGroup.DELETE(buildPath(route.Endpoint, route.Path), func(c *gin.Context) {
		a.handle(route, c)
	})
}

func (a *Application) PUT(route Route) {
	a.EngineGroup.PUT(buildPath(route.Endpoint, route.Path), func(c *gin.Context) {
		a.handle(route, c)
	})
}

func (a *Application) HEAD(route Route) {
	a.EngineGroup.HEAD(buildPath(route.Endpoint, route.Path), func(c *gin.Context) {
		a.handle(route, c)
	})
}

func (a *Application) PATCH(route Route) {
	a.EngineGroup.PATCH(buildPath(route.Endpoint, route.Path), func(c *gin.Context) {
		a.handle(route, c)
	})
}

func (a *Application) OPTIONS(route Route) {
	a.EngineGroup.OPTIONS(buildPath(route.Endpoint, route.Path), func(c *gin.Context) {
		a.handle(route, c)
	})
}

func (a *Application) handle(route Route, c *gin.Context) {
	param := make(map[string]string)
	for n, p := range route.Path {
		param[n] = c.Param(p[1:])
	}

	query := make(map[string]string)
	for n, q := range route.Query {
		value, b := c.GetQuery(q)
		if b {
			query[n] = value
		}
	}

	payload := make(map[string]string)
	if len(route.Payload) > 1 {
		data, _ := ioutil.ReadAll(c.Request.Body)
		payload[route.Payload] = string(data)
	}
	code, response := execute(buildCommand(route.Command, param, query, payload), a.Communication)

	switch route.Output {
	case JSON:
		c.JSON(code, response)
	case YAML:
		c.YAML(code, response)
	case XML:
		c.XML(code, response)
	default:
		c.Render(code, render.String{})
	}
}

func buildPath(endpoint string, path map[string]string) string {
	ret := fmt.Sprintf("%s", endpoint)
	for _, p := range path {
		ret = fmt.Sprintf("%s/%s", ret, p)
	}
	return ret
}

func buildCommand(command string, param map[string]string, query map[string]string, payload map[string]string) []string {
	ret := make([]string, 0)
	ret = append(ret, command)
	for key, value := range param {
		ret = append(ret, fmt.Sprintf("-%s=%s", key, value))
	}
	for key, value := range query {
		ret = append(ret, fmt.Sprintf("-%s=%s", key, value))
	}
	for key, value := range payload {
		ret = append(ret, fmt.Sprintf("-%s=%s", key, value))
	}
	return ret
}

func execute(command []string, output string) (code int, response interface{}) {
	cmd := exec.Command(command[0], command[1:]...)
	var stderr bytes.Buffer
	var stdout bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	cmd.Run()
	var responseStruct struct {
		Code     int         `json:"code"`
		Response interface{} `json:"response"`
	}
	switch output {
	case STDOUT:
		json.Unmarshal(stdout.Bytes(), &responseStruct)
	case STDERR:
		json.Unmarshal(stderr.Bytes(), &responseStruct)
	}
	return responseStruct.Code, responseStruct.Response
}

func main() {
	config := flag.String("config", "./application.json", "application config path")
	flag.Parse()
	app, err := parse(*config)
	if err != nil {
		log.Fatal(err)
	}
	app.start()
}
